#!/usr/bin/python3

#USAGE : python test.py

import cv2
import numpy as np
import time
import Pose_Module as pm
import os
import sys
import paho.mqtt.client as mqtt
import threading
import suggestionsdatabase as sDb

from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.preprocessing import image
from flask import Flask, render_template, Response
from time import sleep
from PyQt5.uic import loadUi
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSlot, QThread, pyqtSignal
from PyQt5.QtWidgets import QApplication, QDialog


class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def run(self):
        #capture from web cam
        cap = cv2.VideoCapture(0)
        #cap = cv2.VideoCapture('right_hand.mp4')

        path_current= os.path.abspath(os.getcwd())

        #load files for emotion recognition
        face_classifier = cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')
        classifier =load_model('./Emotion_Detection.h5')
        class_labels = ['Angry','Happy','Neutral','Sad','Surprise']
        detector = pm.poseDetector()
        count = 0
        dir = 0
        pTime = 0
        emotionLabel =[]
        timeCount=0
        secs=0
        emotionLevel=[]
        actualEmotion=''
        happynessLevelFirst=[]
        happynessLevelEnd  = []
        MainEmotion=[]

        while True: 
            # Grab a single frame of video
            ret, frame = cap.read()
            #counting seconds in the video 
            timeCount+=1
            if (timeCount%30==0):
                secs+=1

            frameWidth= frame.shape[1]
            frameHeight= frame.shape[0]
            os.path.abspath(os.getcwd())

            labels = []
            gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            faces = face_classifier.detectMultiScale(gray,1.3,5)
            frame = cv2.resize(frame, (1280, 720))
            frame = detector.findPose(frame, False)
            lmList = detector.findPosition(frame, False)
            
            ##-------------------------------------------------------------------------   
            ##--------------------------Move recognition----------------------------
            ##-------------------------------------------------------------------------
            if len(lmList) != 0:

                ## Right Arm
                angle,difference = detector.findAngle(frame, 12, 14, 16)
     
                ## extracting minimum and maximum values from the text file
                ## Read text file

                #Minimum value
                f = open(r"angle_file.txt.txt", "r")

                minimum = min(f)

                value_min= int(minimum.replace(',', ''))
                f.close()

                # Maximum value

                f = open(r"angle_file.txt.txt", "r")

                maximum= max(f)
                value_max= int(maximum.replace(',', ''))
                value_max = value_max + 30

                f.close()

                #print("the maximum value",value_max)

                ## Left Arm
                #angle = detector.findAngle(img, 11, 13, 15,False)
                per = np.interp(angle, (value_min, value_max), (0, 100))
                bar = np.interp(angle, (value_min, value_max), (650, 100))
                # print(angle, per)

                # Check for the curls
                color = (255, 0, 255)
                if per == 100:
                    color = (0, 255, 0)
                    if dir == 0:
                        count += 0.5
                        dir = 1
                if per == 0:
                    color = (0, 255, 0)
                    if dir == 1:
                        count += 0.5
                        dir = 0
                #print(count)

                # Draw Bar
                cv2.rectangle(frame, (1100, 100), (1175, 650), color, 3)
                cv2.rectangle(frame, (1100, int(bar)), (1175, 650), color, cv2.FILLED)
                cv2.putText(frame, f'{int(per)} %', (1100, 75), cv2.FONT_HERSHEY_PLAIN, 4,color, 4)

                # Draw Curl Count
                cv2.rectangle(frame, (0, 450), (250, 720), (0, 255, 0), cv2.FILLED)
                cv2.putText(frame, str(int(count)), (45, 670), cv2.FONT_HERSHEY_PLAIN, 15,(255, 0, 0), 25)
                if (count==3):
                    cv2.putText(frame, "Choose an action", (100, 200), cv2.FONT_HERSHEY_PLAIN, 8,(255, 0, 0), 15)
                    cv2.putText(frame, "before continuing", (100, 500), cv2.FONT_HERSHEY_PLAIN, 8,(255, 0, 0), 15)

               
            cTime = time.time()
            fps = 1 / (cTime - pTime)
            pTime = cTime
            cv2.putText(frame, str(int(fps)), (50, 100), cv2.FONT_HERSHEY_PLAIN, 5,
                        (255, 0, 0), 5)
           
            ##-------------------------------------------------------------------------   
            ##--------------------------Emotion recognition----------------------------
            ##-------------------------------------------------------------------------
            for (x,y,w,h) in faces:
                #cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
                roi_gray = gray[y:y+h,x:x+w]
                roi_gray = cv2.resize(roi_gray,(48,48),interpolation=cv2.INTER_AREA)

                if np.sum([roi_gray])!=0:
                    roi = roi_gray.astype('float')/255.0
                    roi = img_to_array(roi)
                    roi = np.expand_dims(roi,axis=0)

                # make a prediction on the ROI, then lookup the class

                    preds = classifier.predict(roi)[0]
                    #print("\nprediction = ",preds)
                    label=class_labels[preds.argmax()]
                    #print("\nprediction max = ",preds.argmax())
                    #print("\nlabel = ",label)

                    label_position = (x,y)
                    cv2.putText(frame,label,label_position,cv2.FONT_HERSHEY_SIMPLEX,2,(0,255,0),3)

                    #knowing avarage of feelings
                    emotionLabel.append(label)

                    #counting emotions
                    Happy = emotionLabel.count('Happy')
                    Sad = emotionLabel.count('Sad')
                    Neutral = emotionLabel.count('Neutral')
                    Angry = emotionLabel.count('Angry')
                    Surprise = emotionLabel.count('Surprise')

                    #percentage emotions
                    HappyLevel = Happy/(len(emotionLabel)) *100
                    Sadlevel = Sad/(len(emotionLabel)) *100
                    SurpriseLevel = Surprise/(len(emotionLabel)) *100
                    AngerLevel = Angry/(len(emotionLabel)) *100
                    NeutralLevel = Neutral/(len(emotionLabel)) *100

                    if (count>=0):
                        emotionLevel=[HappyLevel,Sadlevel,SurpriseLevel,AngerLevel,NeutralLevel]
                        
                        if (max(emotionLevel) == HappyLevel): 
                            actualEmotion='Happy'
                        if(max(emotionLevel)==Sadlevel):
                            actualEmotion ='Sad'
                        if(max(emotionLevel)==SurpriseLevel):
                            actualEmotion='SurpriseLevel'
                        if(max(emotionLevel)==AngerLevel):
                            actualEmotion='Angry'
                        if(max(emotionLevel)==NeutralLevel):
                            actualEmotion='Neutral'  
                        if count==3:
                            MainEmotion.append(actualEmotion)                 

                    #creating mqtt brocker und publishing. 
        
                    if (count>=0 and count<3):                    
                        mqttBroker = "mqtt.eclipseprojects.io"
                        client = mqtt.Client("Test")                       
                        client.connect(mqttBroker)
                       
                        client.publish("EMOTION", str(actualEmotion)+ ","+ str(sDb.CreatingSuggestion(actualEmotion)) )
                        if (HappyLevel==0):
                            happynessLevelFirst.append(0)
                        happynessLevelFirst.append(HappyLevel)

                    if (count>=3 and count<=5):                       
                        mqttBroker = "mqtt.eclipseprojects.io"
                        client = mqtt.Client("Test")                       
                        client.connect(mqttBroker)              
                        client.publish("EMOTION", str(MainEmotion[0])+ ","+ str(sDb.CreatingSuggestion(MainEmotion[0])) )
                        if (HappyLevel==0):
                            happynessLevelFirst.append(0)
                        happynessLevelFirst.append(HappyLevel)

                    if (count>5 ):
                        if (HappyLevel==0):
                            happynessLevelEnd.append(0)
                        happynessLevelEnd.append(HappyLevel)
                        improvement = happynessLevelEnd[0]-happynessLevelFirst[0]
                       
                        mqttBroker = "mqtt.eclipseprojects.io" 
                        client = mqtt.Client("Happy label")
                        client.connect(mqttBroker)
                        client.publish("EMOTION",  str(MainEmotion[0]) +", "+ str(sDb.CreatingSuggestion(actualEmotion)) +", "+ str("{0:.2f}".format(improvement)) + " %")

                else:
                    cv2.putText(frame,'No Face Found',(20,60),cv2.FONT_HERSHEY_SIMPLEX,2,(0,255,0),3)
                print("\n\n")
              
            if ret:
                self.change_pixmap_signal.emit(frame)

  






















