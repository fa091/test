import sys
import os
import numpy as np
import cv2
import datetime
import time
import re
import csv
import paho.mqtt.client as mqtt
from PyQt5 import QtGui
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.uic import loadUi
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSlot, Qt, QDate, QTimer
from PyQt5.QtWidgets import QApplication, QDialog
from twisted.internet import task, reactor
from csv import writer
from recognition import VideoThread


eleccionManager = ""
finalList = []
value=0

class Ui_MainDialog(QDialog):
    def __init__(self):
        super(Ui_MainDialog, self).__init__()

        loadUi("./mainwindow.ui", self)

        self.emotion_label = None 
        self.thread = VideoThread()
        self.thread.change_pixmap_signal.connect(self.update_image)
        self.thread.start()

        #Control close
        self.close_btn.clicked.connect(self.controlClose)
        
        #MQTT connect
        self.mqttBroker = "mqtt.eclipseprojects.io"
        self.mqttBroker2 = "mqtt.eclipseprojects.io"
        self.client = mqtt.Client("Mainwindow")
        self.client.connect(self.mqttBroker)
      
        def first(self):
            global value
            value = 1
        def second(self):
            global value
            value = 2
        def third(self):
            global value
            value = 3
        
        
        self.firstSuggestion.clicked.connect(first)
        self.secondSuggestion.clicked.connect(second)
        self.thirdssuggestion.clicked.connect(third)

        #MQTT on_message recive new emotion
    def on_message(self,client, userdata, message):        
            #print("Received message: ", str(message.payload.decode("utf-8")))
            newEmotion = str(message.payload.decode("utf-8"))
            self.setEmotionLabel(newEmotion)
            
    def setEmotionLabel(self, newEmotion):
        self.emotion_label = newEmotion

    def getEmotionLabel(self):        
        return self.emotion_label

    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)
        self.imgLabel.setPixmap(qt_img)
        
        #MQTT subscribe to topic "EMOTION"
        self.client.loop_start()  
        self.client.subscribe("EMOTION")
        self.client.on_message = self.on_message
        #update new Emotion in GUI

       # self.StatusLabel.setText(self.getEmotionLabel())
        if type(self.getEmotionLabel()) == str:
            part_of_emotions=self.getEmotionLabel().split(',')
            self.emotion_view.setText(str(part_of_emotions[0]))
            

            self.StatusLabel.setText("1. "+ str(part_of_emotions[1])[1:-1] +" \n"+"2. " +str(part_of_emotions[2])[1:-1] +" \n"
                +"3. " +str(part_of_emotions[3])[1:-1]+ " \n")
            
            #self.StatusLabel.setText(self.trocitosdeEmocion[2]
            #self.StatusLabel.setText(self.trocitosdeEmocion[3]
            
        if(value!=0):
            part_of_emotions=self.getEmotionLabel().split(',')


            global eleccionManager
            eleccionManager=part_of_emotions[value]
            eleccionManager = re.sub(r"[^a-zA-Z0-9- -]","",eleccionManager)
            
            if len (part_of_emotions)>=5:
                finalList.append(part_of_emotions[0])
                finalList.append(eleccionManager)
                finalList.append(part_of_emotions[4])
                if len(finalList)!=0 and len(finalList)<=3:
                    with open('ResultsDataBase.csv', 'a') as f_object:
                            writer_object = writer(f_object,delimiter = ',')
                            writer_object.writerow([str(finalList[0]) + str(finalList[1] + str(finalList[2]))])
                            f_object.close()
            #update new Emotion in GUI
      
        self.client.loop_stop()

    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        self.display_width = 781
        self.display_height = 601

        p = convert_to_Qt_format.scaled(self.display_width, self.display_height, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)

    def controlClose(self):
        os._exit(1)

      




   
 