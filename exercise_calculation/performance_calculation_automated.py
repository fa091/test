
# coding: utf-8

# In[3]:


import cv2
import numpy as np
import time
import Pose_Module as pm

import os


path_current= os.path.abspath(os.getcwd())


## Reading video

#cap = cv2.VideoCapture(r"../videos/right_hand.mp4") #capture by a video file 
cap = cv2.VideoCapture(0) # capture by your own camera

#cap = cv2.VideoCapture(0)
hasFrame, framee =cap.read()
frameeWidth= framee.shape[1]
frameeHeight= framee.shape[0]
os.path.abspath(os.getcwd())



#vid_writer= cv2.VideoWriter(path_current+ '\\prediction\hand_angle.avi', cv2.VideoWriter_fourcc('M','J','P','G'),5, (framee.shape[1],framee.shape[0]))
detector = pm.poseDetector()
count = 0
dir = 0
pTime = 0
while True:
	success, img = cap.read()
	img = cv2.resize(img, (1280, 720))
	# img = cv2.imread("AiTrainer/test.jpg")
	img = detector.findPose(img, False)
	lmList = detector.findPosition(img, False)
	# print(lmList)
	if len(lmList) != 0:
		# Right Arm
		angle,difference = detector.findAngle(img, 12, 14, 16)


		## extracting minimum and maximum values from the text file
		## Read text file

		#Minimum value
		f = open(r"../precalibration/angle_file.txt", "r")

		minimum = min(f)

		value_min= int(minimum.replace(',', ''))
		f.close()

		# Maximum value

		f = open(r"../precalibration/angle_file.txt", "r")

		maximum= max(f)
		value_max= int(maximum.replace(',', ''))
		value_max = value_max + 30

		f.close()

		#print("the maximum value",value_max)



		# # Left Arm
		#angle = detector.findAngle(img, 11, 13, 15,False)
		per = np.interp(angle, (value_min, value_max), (0, 100))
		bar = np.interp(angle, (value_min, value_max), (650, 100))
		# print(angle, per)

		# Check for the curls
		color = (255, 0, 255)
		if per == 100:
			color = (0, 255, 0)
			if dir == 0:
				count += 0.5
				dir = 1
		if per == 0:
			color = (0, 255, 0)
			if dir == 1:
				count += 0.5
				dir = 0
		#print(count)

		# Draw Bar
		cv2.rectangle(img, (1100, 100), (1175, 650), color, 3)
		cv2.rectangle(img, (1100, int(bar)), (1175, 650), color, cv2.FILLED)
		cv2.putText(img, f'{int(per)} %', (1100, 75), cv2.FONT_HERSHEY_PLAIN, 4,
					color, 4)

		# Draw Curl Count
		cv2.rectangle(img, (0, 450), (250, 720), (0, 255, 0), cv2.FILLED)
		cv2.putText(img, str(int(count)), (45, 670), cv2.FONT_HERSHEY_PLAIN, 15,
					(255, 0, 0), 25)

	cTime = time.time()
	fps = 1 / (cTime - pTime)
	pTime = cTime
	cv2.putText(img, str(int(fps)), (50, 100), cv2.FONT_HERSHEY_PLAIN, 5,
				(255, 0, 0), 5)
	#vid_writer.write(img)


	# calcuates the z axis i.e. the posture of the hand_angle


	if difference >0.35:
		img = cv2.applyColorMap(img, cv2.COLORMAP_AUTUMN)
		cv2.putText(img, str("please correct posture"), (50, 100), cv2.FONT_HERSHEY_PLAIN, 5,
				(255, 0, 0), 5)
		cv2.imshow("Image", img)
		#print("PLEASE PRINT difference",difference)

	else:

		cv2.imshow("Image", img)

	#cv2.waitKey(1)


	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()
