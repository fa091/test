#!/usr/bin/python3
import sys
from PyQt5.uic import loadUi
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QDialog

from mainwindow import Ui_MainDialog

class Ui_Dialog(QDialog):
    def __init__(self):
        super(Ui_Dialog, self).__init__()
        loadUi("start_window.ui", self)
        

        self.runButton.clicked.connect(self.runSlot)

        self._new_window = None
        #self.Videocapture_ = None

    def refreshAll(self):
        """
        Set the text of lineEdit once it's valid
        """
        self.Videocapture_ = "0"

    @pyqtSlot()
    def runSlot(self):
        """
        Called when the user presses the Run button
        """
        print("Clicked Run")
        ui.hide()  # hide the main window
        self.mainOutWindow_()  # Create and open new output window

    def mainOutWindow_(self):
        """
        Created new window for vidual output of the video in GUI
        """
        self._new_window = Ui_MainDialog()
        self._new_window.show()
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    ui = Ui_Dialog()
    ui.show()
    sys.exit(app.exec_())
